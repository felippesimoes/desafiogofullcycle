FROM golang:1.15 AS builder

WORKDIR /go/src/desafioGo
COPY . .
RUN GOOS=linux go build main.go

FROM scratch
WORKDIR /go/src/desafioGo
COPY --from=builder /go/src/desafioGo .
EXPOSE 9090

ENTRYPOINT ["./main"]
CMD [ "go", "run" "main.go" ]

